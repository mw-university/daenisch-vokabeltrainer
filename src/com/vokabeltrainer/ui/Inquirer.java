package com.vokabeltrainer.ui;

import com.vokabeltrainer.data.models.Choice;
import com.vokabeltrainer.data.models.Question;

import java.util.Scanner;

public class Inquirer {
    private static Scanner sc = new Scanner(System.in);

    public Choice askTranslation(Question question, boolean anonymousCase) {
        print(String.format("\n> Translate: '%s'", question.getDanishWord()));

//        printEmptyLine();

        print("Which of these words is the correct translation?");
        for (int i = 0; i < question.getChoices().extent(); i++) {
            print(String.format("%d) %s", (i + 1),
                    anonymousCase ? question.getChoices().at(i).getWord().toLower() : question.getChoices().at(i).getWord()
            ));
        }

        printEmptyLine();

        int result = 0;
        while (result == 0) {
            String validationError = String.format("Please enter a number from 1 - %d!", question.getChoices().extent());
            System.out.print("Answer: ");

            // Validate: is integer
            boolean inputIsInteger = this.sc.hasNextInt();
            if (inputIsInteger) {
                // Validate: value is allowed
                int userInput = this.sc.nextInt();
                if (userInput <= question.getChoices().extent() && userInput > 0) {
                    result = userInput;
                }
            }

            // No result has been found -> continue
            if (result == 0) {
                print(validationError);
                this.sc.next();
            }
        }

        return question.getChoices().at(result - 1);
    }

    private static void printEmptyLine() {
        print("");
    }

    private static void print(String message) {
        System.out.println(message);
    }
}
