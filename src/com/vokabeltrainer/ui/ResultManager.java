package com.vokabeltrainer.ui;

import com.vokabeltrainer.data.models.Choice;
import com.vokabeltrainer.data.models.Result;
import lingologs.Script;

import java.text.DecimalFormat;

public class ResultManager {
    private Result result = new Result();

    public void printResult(Choice userChoice) {
        Script result = userChoice.isCorrectAnswer() ? new Script("correct") : new Script("incorrect");
        System.out.println(String.format("\nYour answer is %s!", result));
    }

    public void printEndscore() {
        System.out.println("\n=== Results ===");
        System.out.println("Correct answers: " + result.getCorrectAnswers());
        System.out.println("Wrong answers: " + result.getWrongAnswers());

        double resultPercentage = (double)result.getCorrectAnswers() / (double)(result.getWrongAnswers() + result.getCorrectAnswers()) * 100.0d;
        String formattedPercentage = new DecimalFormat("#.##").format(resultPercentage);
        System.out.println("Percentage: " + formattedPercentage + "%");
    }

    public void countCorrectAnswer() {
        result.countCorrectAnswer();
    }

    public void countWrongAnswer() {
        result.countWrongAnswer();
    }
}
