package com.vokabeltrainer.data.models;

public enum WordFinderAlgorithm {
    Levenshtein,
    SorensenDice,
    Jaccard,
    Overlap,
    SoundexLevenshtein,
    SoundexSorensenDice,
    SoundexJaccard,
    SoundexOverlap,
    Soundex,
    Shuffled,
    Contained,
    Reversed, // x
    IntroExtroLength,
    Rotated, // x
}
