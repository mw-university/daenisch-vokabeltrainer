package com.vokabeltrainer.data.models;

import lingologs.Script;
import lingologs.Texture;

public class Question {
    private Script danishWord;
    private Texture<Choice> choices = new Texture<>();

    public Question(Script danishWord, Script correctAnswer, Texture<Script> alternativeChoices) {
        this.danishWord = danishWord;

        // Add correct choice
        this.choices = this.choices.add(new Choice(correctAnswer, true));

        // Add wrong choices
        for (Script choice : alternativeChoices) {
            this.choices = this.choices.add(new Choice(choice, false));
        }

        // Randomize order
        this.choices = this.choices.shuffle();
    }

    public Script getDanishWord() {
        return danishWord;
    }

    public Texture<Choice> getChoices() {
        return choices;
    }
}
