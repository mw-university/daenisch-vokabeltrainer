package com.vokabeltrainer.data.models;

public class Result {
    private int correctAnswers = 0;
    private int wrongAnswers = 0;

    public void countCorrectAnswer() {
        correctAnswers ++;
    }

    public void countWrongAnswer() {
        wrongAnswers ++;
    }

    public int getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(int correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public int getWrongAnswers() {
        return wrongAnswers;
    }

    public void setWrongAnswers(int wrongAnswers) {
        this.wrongAnswers = wrongAnswers;
    }
}

