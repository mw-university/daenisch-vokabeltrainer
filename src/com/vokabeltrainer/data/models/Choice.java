package com.vokabeltrainer.data.models;

import lingologs.Script;

public class Choice {
    private Script word;
    private Boolean isCorrectAnswer;

    public Choice(Script choice, Boolean isCorrectAnswer) {
        this.word = choice;
        this.isCorrectAnswer = isCorrectAnswer;
    }

    public Script getWord() {
        return word;
    }

    public Boolean isCorrectAnswer() {
        return isCorrectAnswer;
    }
}
