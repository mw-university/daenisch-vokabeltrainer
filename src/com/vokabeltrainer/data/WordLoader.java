package com.vokabeltrainer.data;

import lingolava.Tuple.Couple;
import lingologs.Charact;
import lingologs.Script;
import lingologs.Texture;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class WordLoader {
    public Texture<Script> loadDanishWords() {
        String pathname = "/src/com/vokabeltrainer/input/da_DK.csv";
        String filePath = new File("").getAbsolutePath() + pathname;

        // Builder
        Texture.Builder<Script> result = new Texture.Builder<>();
        try {
            Scanner sc = new Scanner(new File(filePath));
            while(sc.hasNext())
            {
                Script word = new Script (sc.nextLine());
                result = result.add(word);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Couldn't load danish words file.");
        }

        return result.toTexture();
    }

    public Texture<Couple<Script, Script>> loadVocabulary() {
        String pathname = "/src/com/vokabeltrainer/input/de_dk.csv";
        String filePath = new File("").getAbsolutePath() + pathname;

        // Builder
        Texture.Builder<Couple<Script, Script>> result = new Texture.Builder<>();
        try {
            Scanner sc = new Scanner(new File(filePath));
            while(sc.hasNext())
            {
                Script line = new Script (sc.nextLine());
                List<Script> components = line.split(Charact.of(';'));
                if (components.size() > 1) {
                    result = result.add(new Couple<>(components.get(0), components.get(1)));
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Couldn't load vocabulary file.");
        }

        return result.toTexture();
    }
}
