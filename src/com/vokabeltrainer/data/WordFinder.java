package com.vokabeltrainer.data;

import com.vokabeltrainer.data.models.WordFinderAlgorithm;
import lingolava.Legacy.Similitude;
import lingolava.Tuple.Couple;
import lingologs.Script;
import lingologs.Texture;

import java.util.Comparator;
import java.util.Random;

public class WordFinder {
    private Texture<Script> danishWords;
    private Texture<Couple<Script, Script>> vocabulary;
    private Random random;

    public WordFinder() {
        this.random = new Random();

        WordLoader wl = new WordLoader();
        this.danishWords = wl.loadDanishWords();
        this.vocabulary = wl.loadVocabulary();
    }

    public Couple<Script, Script> getRandomVocabulary() {
        return vocabulary.at(random.nextInt(vocabulary.extent()));
    }

    public Texture<Script> findSimilar(Script danishWord, WordFinderAlgorithm algorithm, int amount) {
        Texture<Script> result;

        switch (algorithm) {
            // Similtudes
            case Levenshtein -> result = findSimilares(danishWord, Similitude.Leven, false, amount);
            case SorensenDice -> result = findSimilares(danishWord, Similitude.SorenDice, false ,amount);
            case Jaccard -> result = findSimilares(danishWord, Similitude.Jaccard, false, amount);
            case Overlap -> result = findSimilares(danishWord, Similitude.Overlap, false, amount);
            // Soundex
            case Soundex -> result = findIdenticalSoundex(danishWord, amount);
            case SoundexLevenshtein -> result = findSimilares(danishWord, Similitude.Leven, true, amount);
            case SoundexSorensenDice -> result = findSimilares(danishWord, Similitude.SorenDice, true,amount);
            case SoundexJaccard -> result = findSimilares(danishWord, Similitude.Jaccard, true, amount);
            case SoundexOverlap -> result = findSimilares(danishWord, Similitude.Overlap, true, amount);
            // Other comparisons
            case Shuffled -> result = findShuffled(danishWord, amount);
            case Contained -> result = findContained(danishWord, amount);
            case IntroExtroLength -> result = findIdenticalIntroExtroLength(danishWord, amount);
            case Rotated -> result = findRotated(danishWord, amount);

    //            case Reversed -> result = findSimilarReversed(danishWord);

            default -> result = new Texture<>();
        }

        return result;
    }

    private Texture<Script> findSimilares(
            Script danishWord,
            Similitude algorithm,
            boolean compareSoundex,
            int amount
    ) {
        // Calculate and save all similtudes
        // Texture<Couple<Script, Double>> similtudeResults = new Texture<>();
        Texture.Builder<Couple<Script, Double>> similtudeResults = new Texture.Builder<>();

        // Foreach?
        for (int i = 0; i < danishWords.extent(); i++) {
            Script currentWord = danishWords.at(i);
            if (!currentWord.equals(danishWord)) {
                double currentSimiltude;
                if (compareSoundex) {
                    // Dänischer soundex?
                    currentSimiltude = danishWord.toSoundex().similares(currentWord.toSoundex(), algorithm);
                } else {
                    currentSimiltude = danishWord.similares(currentWord, algorithm);
                }

                similtudeResults = similtudeResults.add(new Couple<>(currentWord, currentSimiltude));
            }
        }

        // Sort similtudes
        // similtudeResults = similtudeResults.sort(Comparator.comparing(Couple::it1));
        Texture<Couple<Script, Double>> similtudeResultsOrdered = similtudeResults.toTexture().sort(Comparator.comparing(Couple::it1));


        // Select highest results
        Texture<Script> results = new Texture<>();
        if (similtudeResultsOrdered.extent() > 0) {
            for (int i = similtudeResultsOrdered.extent() - 1; i > similtudeResultsOrdered.extent() - amount - 1; i--) {
                results = results.add(similtudeResultsOrdered.at(i).it0());
            }
        }

        return results;
    }

    private Texture<Script> findShuffled(Script danishWord, int amount) {
        Texture<Script> results = new Texture<>();

        // Find the first x words which are shuffled
        for (int i = 0; i < danishWords.extent(); i++) {
            Script currentWord = danishWords.at(i);

            if (!danishWord.equals(currentWord)
                    && danishWord.isShuffled(currentWord)
                    // && !results.contains(currentWord)
            ) {
                results = results.add(currentWord);
                if (results.extent() == amount) {
                    break;
                }
            }
        }

        return results;
    }

    private Texture<Script> findContained(Script danishWord, int amount) {
        Texture<Script> results = new Texture<>();

        // Find the first x words which contain another
        for (int i = 0; i < danishWords.extent(); i++) {
            Script currentWord = danishWords.at(i);

            if (!danishWord.equals(currentWord)
                    && currentWord.length() > 2
                    && danishWord.contains(currentWord)
                    || currentWord.contains(danishWord)) {
                results = results.add(currentWord);
                if (results.extent() == amount) {
                    break;
                }
            }
        }

        return results;
    }

    private Texture<Script> findIdenticalIntroExtroLength(Script danishWord, int amount) {
        Texture<Script> results = new Texture<>();

        // Find the first x words which have same length, start and end character
        for (int i = 0; i < danishWords.extent(); i++) {
            Script currentWord = danishWords.at(i);

            int nChars = 1;
            if (!danishWord.equals(currentWord)
                    && currentWord.intro(nChars).equals(danishWord.intro(nChars))
                    && currentWord.length() == danishWord.length()
                    && currentWord.extro(nChars).equals(danishWord.extro(nChars))
                    && !results.contains(currentWord)
            ) {
                results = results.add(currentWord);
                if (results.extent() == amount) {
                    break;
                }
            }
        }

        return results;
    }

    private Texture<Script> findIdenticalSoundex(Script danishWord, int amount) {
        Texture<Script> results = new Texture<>();

        // Find the first x words which have identical soundex
        for (int i = 0; i < danishWords.extent(); i++) {
            Script currentWord = danishWords.at(i);

            int nChars = 1;
            if (!danishWord.equals(currentWord)
                    && danishWord.toSoundex().equals(currentWord.toSoundex())
                    && !results.contains(currentWord)
            ) {
                results = results.add(currentWord);
                if (results.extent() == amount) {
                    break;
                }
            }
        }

        return results;
    }

    private Texture<Script> findRotated(Script danishWord, int amount) {
        Texture<Script> results = new Texture<>();

        for (int i = 0; i < danishWords.extent(); i++) {
            Script currentWord = danishWords.at(i);

            int nChars = 1;
            if (!danishWord.equals(currentWord)
                    && danishWord.isRotated(currentWord)
                    && !results.contains(currentWord)
            ) {
                results = results.add(currentWord);
                if (results.extent() == amount) {
                    break;
                }
            }
        }

        return results;
    }

//    private Texture<Script> findSimilarReversed(Script danishWord) {
//        Texture<Script> results = new Texture<>();
//
//        // Find the first x words which contain another
//        for (int i = 0; i < danishWords.extent(); i++) {
//            Script currentWord = danishWords.at(i);
//
//            if (!danishWord.isReverse(currentWord)) {
//                results = results.add(currentWord);
//                break;
//            }
//        }
//
//        return results;
//    }
}
