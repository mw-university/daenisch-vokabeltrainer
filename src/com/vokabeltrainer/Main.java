package com.vokabeltrainer;

import com.vokabeltrainer.data.WordFinder;
import com.vokabeltrainer.data.models.Choice;
import com.vokabeltrainer.data.models.Question;
import com.vokabeltrainer.data.models.WordFinderAlgorithm;
import com.vokabeltrainer.ui.Inquirer;
import com.vokabeltrainer.ui.ResultManager;
import lingolava.Tuple.Couple;
import lingologs.Script;
import lingologs.Texture;

public class Main {

    public static void main(String[] args) {
        // Setup input data
        System.out.println("Loading words...");
        WordFinder wordFinder = new WordFinder();

        // Generate questions
        int questionsAmount = 5;
        WordFinderAlgorithm selectionAlgorithm = WordFinderAlgorithm.Levenshtein;

        Texture<Question> questions = new Texture<>();
        System.out.println("Generating " + questionsAmount + " questions...");
        for (int i = 0; i < 5; i++) {
            int minAlternativeChoices = 1; // Only questions with at least 1 choice will be selected
            int maxAlternativeChoices = 4;

            boolean questionCreated = false;
            while (!questionCreated) {
                // Todo: Change Couple -> Vocabulary class
                Couple<Script, Script> randomVocabulary = wordFinder.getRandomVocabulary();
                Texture<Script> choices = wordFinder.findSimilar(randomVocabulary.it0(), selectionAlgorithm, maxAlternativeChoices);

                if (choices.extent() > minAlternativeChoices){
                    questions = questions.add(new Question(randomVocabulary.it1(), randomVocabulary.it0(), choices));
                    questionCreated = true;
                }
            }
        }

        // Pose questions to the user
        Inquirer i = new Inquirer();
        ResultManager rm = new ResultManager();

        for (Question question : questions) {
            // Anonymous case displays all choices in lowercase
            Choice result = i.askTranslation(question, false);

            // Print feedback (correct / incorrect)
            rm.printResult(result);

            // Count score
            if (result.isCorrectAnswer()) {
                rm.countCorrectAnswer();
            } else {
                rm.countWrongAnswer();
            }
        }

        // Print end-score
        rm.printEndscore();
    }
}
