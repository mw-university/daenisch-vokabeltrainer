Zum Ausführen des Projekts muss `LingoLibry.jar` zum Projekt hinzugefügt werden.

Der Datensatz [da_DK.csv](https://gitlab.com/mw-university/daenisch-vokabeltrainer/-/blob/master/src/com/vokabeltrainer/input/da_DK.csv) wurde auf [Kaggle](https://www.kaggle.com/datasets/jacekpardyak/languages-of-europe?select=da_DK.csv) gefunden.
